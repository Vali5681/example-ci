#include <iostream>
#include "calculator.hpp"

enum operand_t{
    id_plus,
    id_minus,
    id_product,
    id_divide,
    id_sqrt
};

operand_t hash(std::string const& command)
{
    if (command == "+") return id_plus;
    if (command == "-") return id_minus;
    if (command == "*") return id_product;
    if (command == "/") return id_divide;
    if (command == "sqrt") return id_sqrt;
}

int main() {
    auto calc = calculator();
    auto result = 0.0;
    std::cout << "Please, choose the type of operation that you want to be performed from following list: \n";
    std::cout << "+\n-\n*\n/\nsqrt\n";
    std::cout << "Insert the operator: \n";
    std::string operand;
    std::cin >> operand;
    std::cout << "Do you want the program to use variables of type int or double?\nType 0 for int, or other number for double.\n";
    int option = 0;
    std::cin >> option;
    std::cout << "Input the variable(s):\n";
    double operator1;
    double operator2;
    std::cin >> operator1;
    if (hash(operand) != id_sqrt)
        std::cin >> operator2;
    if (option == 0)
    {
        operator1 = (int)operator1;
        operator2 = (int)operator2;
    }
    switch(hash(operand))
    {
        case id_plus:
            result = calc.add(operator1, operator2);
            break;
        case id_minus:
            result = calc.sub(operator1, operator2);
            break;
        case id_product:
            result = calc.product(operator1, operator2);
            break;
        case id_divide:
            result = calc.divide(operator1, operator2);
            break;
        case id_sqrt:
            result = calc.sqrt(operator1);
            break;
    }
    std::cout << "The result is the following: " << result << "\n";
    return 0;
}