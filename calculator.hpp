//
// Created by valen on 10/8/2018.
//

#ifndef ASSIG_UNIT_TESTING_CALCULATOR_H
#define ASSIG_UNIT_TESTING_CALCULATOR_H

class calculator{
public:
    calculator();
    int add(int operator1, int operator2);
    double add(double operator1, double operator2);
    int sub(int operator1, int operator2);
    double sub(double operator1, double operator2);
    int product(int operator1, int operator2);
    double product(double operator1, double operator2);
    int divide(int operator1, int operator2);
    double divide(double operator1, double operator2);
    double sqrt(int operator1);
    double sqrt(double operator1);
private:
    double result;
    bool logic_error;
};

#endif //ASSIG_UNIT_TESTING_CALCULATOR_H
