//
// Created by valen on 10/8/2018.
//

#include <gtest/gtest.h>
#include "../calculator.hpp"
#include "../calculator.cpp"

TEST(calculator_int_tests, add_test)
{
    calculator calc;
    int result = calc.add(9,6);
    std::cout << "Result: " << result << '\n';
    std::cout << "Reference: 15\n";
    ASSERT_EQ(result, 15);
}

TEST(calculator_int_tests, sub_test)
{
    calculator calc;
    int result = calc.sub(9,6);
    std::cout << "Result: " << result << '\n';
    std::cout << "Reference: 3\n";
    ASSERT_EQ(result, 3);
}

TEST(calculator_int_tests, product_test)
{
    calculator calc;
    int result = calc.product(2,3);
    std::cout << "Result: " << result << '\n';
    std::cout << "Reference: 6\n";
    ASSERT_EQ(result, 6);
}

TEST(calculator_int_tests, divide_test)
{
    calculator calc;
    int result = calc.divide(6,2);
    std::cout << "Result: " << result << '\n';
    std::cout << "Reference: 3\n";
    ASSERT_EQ(result, 3);
}

TEST(calculator_int_tests, sqrt_test)
{
    calculator calc;
    int result = calc.sqrt(4);
    std::cout << "Result: " << result << '\n';
    std::cout << "Reference: 2\n";
    ASSERT_EQ(result, 2);
}

TEST(calculator_double_tests, add_test)
{
    calculator calc;
    double result = calc.add(0.1,0.4);
    std::cout << "Result: " << result << '\n';
    std::cout << "Reference: 0.5\n";
    ASSERT_EQ(result, 0.5);
}

TEST(calculator_double_tests, sub_test)
{
    calculator calc;
    double result = calc.sub(0.2,0.1);
    std::cout << "Result: " << result << '\n';
    std::cout << "Reference: 0.1\n";
    ASSERT_EQ(result, 0.1);
}
TEST(calculator_double_tests, product_test)
{
    calculator calc;
    double result = calc.product(0.2,0.2);
    std::cout << "Result: " << result << '\n';
    std::cout << "Reference: 0.04\n";
    double reference = 0.2 * 0.2;
    ASSERT_EQ(result, reference);
}
TEST(calculator_double_tests, divide_test) {
    calculator calc;
    double result = calc.divide(0.1, 0.1);
    std::cout << "Result: " << result << '\n';
    std::cout << "Reference: 1\n";
    ASSERT_EQ(result, 1);
}
TEST(calculator_double_tests, sqrt_test) {
    calculator calc;
    double result = calc.sqrt(0.04);
    std::cout << "Result: " << result << '\n';
    std::cout << "Reference: 0.2\n";
    ASSERT_EQ(result, 0.2);
}
TEST(calculator_int_test, error_test)
{
    auto calc = calculator();
    // this tests _that_ the expected exception is thrown
    EXPECT_THROW({ try
                     {
                         calc.divide(2,0);
                     }
                     catch(const std::exception& e)
                     {
                         // and this tests that it has the correct message
                         EXPECT_STREQ("Division by Zero is impossible.", e.what());
                         throw;
                     }
                 }, std::logic_error);
}
