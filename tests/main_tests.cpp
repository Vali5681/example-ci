//
// Created by valen on 10/8/2018.
//

#include <gtest/gtest.h>
#include <iostream>

int main(int argc, char** argv)
{
    std::cout << "Running the main test.\n";
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}