//
// Created by valen on 10/8/2018.
//

#include "calculator.hpp"
#include <cmath>
#include <stdexcept>

calculator::calculator() : result{0} {}

int calculator::add(int operator1, int operator2) {
    return operator1 + operator2;
}

double calculator::add(double operator1, double operator2) {
    return operator1 + operator2;
}

int calculator::sub(int operator1, int operator2) {
    return operator1 - operator2;
}

double calculator::sub(double operator1, double operator2) {
    return operator1 - operator2;
}

int calculator::product(int operator1, int operator2) {
    return operator1 * operator2;
}

double calculator::product(double operator1, double operator2) {
    return operator1 * operator2;
}

int calculator::divide(int operator1, int operator2) {
    if(operator2 == 0)
        throw std::logic_error("Division by Zero is impossible.");
    return operator1 / operator2;
}

double calculator::divide(double operator1, double operator2) {
    if(operator2 == 0)
        throw std::logic_error("Division by Zero is impossible.");
    return operator1 / operator2;
}

double calculator::sqrt(int operator1) {
    return sqrtf(operator1);
}

double calculator::sqrt(double operator1) {
    return sqrtl(operator1);
}
